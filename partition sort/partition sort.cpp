// partition sort.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "iostream"
#include "vector"
#include <ctime>

template<typename _RandomAccessIterator>
inline void
__pop_heap(_RandomAccessIterator __first, _RandomAccessIterator __last,
	_RandomAccessIterator __result)
{
	typedef typename std::iterator_traits<_RandomAccessIterator>::value_type
		_ValueType;
	typedef typename std::iterator_traits<_RandomAccessIterator>::difference_type
		_DistanceType;

	_ValueType __value = std::move(*__result);
	*__result = std::move(*__first);
	__adjust_heap(__first, _DistanceType(0),
		_DistanceType(__last - __first),
		std::move(__value));
}


template<typename _RandomAccessIterator, typename _Distance, typename _Tp>
void   __push_heap(_RandomAccessIterator __first,
	_Distance __holeIndex, _Distance __topIndex, _Tp __value)
{
	_Distance __parent = (__holeIndex - 1) / 2;
	while (__holeIndex > __topIndex && *(__first + __parent) < __value)
	{
		*(__first + __holeIndex) = std::move(*(__first + __parent));
		__holeIndex = __parent;
		__parent = (__holeIndex - 1) / 2;
	}
	*(__first + __holeIndex) = std::move(__value);
}

template<typename _RandomAccessIterator, typename _Distance, typename _Tp>
void
__adjust_heap(_RandomAccessIterator __first, _Distance __holeIndex,
	_Distance __len, _Tp __value)
{
	const _Distance __topIndex = __holeIndex;
	_Distance __secondChild = __holeIndex;
	while (__secondChild < (__len - 1) / 2)
	{
		__secondChild = 2 * (__secondChild + 1);
		if (*(__first + __secondChild) < *(__first + (__secondChild - 1)))
			__secondChild--;
		*(__first + __holeIndex) = std::move(*(__first + __secondChild));
		__holeIndex = __secondChild;
	}
	if ((__len & 1) == 0 && __secondChild == (__len - 2) / 2)
	{
		__secondChild = 2 * (__secondChild + 1);
		*(__first + __holeIndex) = std::move(*(__first
			+ (__secondChild - 1)));
		__holeIndex = __secondChild - 1;
	}
	__push_heap(__first, __holeIndex, __topIndex,
		std::move(__value));
}



template<typename _RandomAccessIterator>
void make_heap(_RandomAccessIterator __first, _RandomAccessIterator __last)
{
	typedef typename iterator_traits<_RandomAccessIterator>::value_type
		_ValueType;
	typedef typename iterator_traits<_RandomAccessIterator>::difference_type
		_DistanceType;

	if (__last - __first < 2)
		return;

	const _DistanceType __len = __last - __first;
	_DistanceType __parent = (__len - 2) / 2;
	while (true)
	{
		_ValueType __value = std::move(*(__first + __parent));
		__adjust_heap(__first, __parent, __len, _GLIBCXX_MOVE(__value), __comp);
		if (__parent == 0)
			return;
		__parent--;
	}
}


template <class _RandomAccessIterator>
void
__make_heap(_RandomAccessIterator __first, _RandomAccessIterator __last)
{
	typedef typename std::iterator_traits<_RandomAccessIterator>::difference_type difference_type;
	difference_type __n = __last - __first;
	if (__n > 1)
	{
		__last = __first;
		++__last;
		for (difference_type __i = 1; __i < __n;)
			__push_heap_back(__first, ++__last, ++__i);
	}
}

template <class _RandomAccessIterator>
void
__push_heap_back(_RandomAccessIterator __first, _RandomAccessIterator __last,
	size_t __len)
{
	typedef typename std::iterator_traits<_RandomAccessIterator>::difference_type difference_type;
	typedef typename std::iterator_traits<_RandomAccessIterator>::value_type value_type;
	if (__len > 1)
	{
		__len = (__len - 2) / 2;
		_RandomAccessIterator __ptr = __first + __len;
		if ((*__ptr > *--__last))
		{
			value_type __t(std::move(*__last));
			do
			{
				*__last = std::move(*__ptr);
				__last = __ptr;
				if (__len == 0)
					break;
				__len = (__len - 1) / 2;
				__ptr = __first + __len;
			} while (*__ptr > __t);
			*__last = std::move(__t);
		}
	}
}


template<typename _RandomAccessIterator>
void
__heap_select(_RandomAccessIterator __first,
	_RandomAccessIterator __middle,
	_RandomAccessIterator __last)
{
	__make_heap(__first, __middle);
	for (_RandomAccessIterator __i = __middle; __i < __last; ++__i)
		if (*__i > *__first)
			__pop_heap(__first, __middle, __i);
}


template<typename _RandomAccessIterator>
void
__sort_heap(_RandomAccessIterator __first, _RandomAccessIterator __last)
{
	while (__last - __first > 1)
	{
		--__last;
		__pop_heap(__first, __last, __last);
	}
}

template<typename _RandomAccessIterator>
inline void
__partial_sort(_RandomAccessIterator __first,
	_RandomAccessIterator __middle,
	_RandomAccessIterator __last)
{
	__heap_select(__first, __middle, __last);
	__sort_heap(__first, __middle);
	//std::cout << std::endl;
	//for (auto z = __first; __first != __last; ++z)
	//	std::cout << (*z) << "ewwerwerwerwrwrwrwe ";
	//std::cout << std::endl;
}






template<typename _Iterator>
void
__move_median_to_first(_Iterator __result, _Iterator __a, _Iterator __b, _Iterator __c)
{
	if (*__a > *__b)
	{
		if (*__b > *__c)
			iter_swap(__result, __b);
		else if (*__a > * __c)
			iter_swap(__result, __c);
		else
			iter_swap(__result, __a);
	}
	else if (*__a > * __c)
		iter_swap(__result, __a);
	else if (*__b > * __c)
		iter_swap(__result, __c);
	else
		iter_swap(__result, __b);
}


template<typename _RandomAccessIterator>
_RandomAccessIterator
__unguarded_partition(_RandomAccessIterator __first,
	_RandomAccessIterator __last,
	_RandomAccessIterator __pivot)
{
	while (true)
	{
		while (*__first > *__pivot)
			++__first;
		--__last;
		while (*__pivot > *__last)
			--__last;
		if (!(__first < __last))
			return __first;
		iter_swap(__first, __last);
		++__first;
	}
}

template<typename _RandomAccessIterator>
inline _RandomAccessIterator
__unguarded_partition_pivot(_RandomAccessIterator __first,
	_RandomAccessIterator __last)
{
	_RandomAccessIterator __mid = __first + (__last - __first) / 2;
	__move_median_to_first(__first, __first + 1, __mid, __last - 1);
	return  __unguarded_partition(__first + 1, __last, __first);
}


enum { _S_threshold = 16 };




template<typename _RandomAccessIterator, typename _Size>
void
__introsort_loop(_RandomAccessIterator __first,
	_RandomAccessIterator __last,
	_Size __depth_limit)
{
	while (__last - __first > int(_S_threshold))
	{
		if (__depth_limit == 0)
		{
			__partial_sort(__first, __last, __last);
			return;
		}
		--__depth_limit;
		_RandomAccessIterator __cut = __unguarded_partition_pivot(__first, __last);
		__introsort_loop(__cut, __last, __depth_limit);
		__last = __cut;
	}
}

template<typename _RandomAccessIterator>
void
__unguarded_linear_insert(_RandomAccessIterator __last)
{
	typename std::iterator_traits<_RandomAccessIterator>::value_type
		__val = std::move(*__last);
	_RandomAccessIterator __next = __last;
	--__next;
	while (__val > *__next)
	{
		*__last = std::move(*__next);
		__last = __next;
		--__next;
	}
	*__last = std::move(__val);
}


template<typename _RandomAccessIterator>
inline void
__unguarded_insertion_sort(_RandomAccessIterator __first,
	_RandomAccessIterator __last)
{
	for (_RandomAccessIterator __i = __first; __i != __last; ++__i)
		__unguarded_linear_insert(__i);
}

template<typename _RandomAccessIterator>
void
__insertion_sort(_RandomAccessIterator __first,
	_RandomAccessIterator __last)
{
	if (__first == __last) return;

	for (_RandomAccessIterator __i = __first + 1; __i != __last; ++__i)
	{
		auto  __val = *__i;
		if ((*__i > *__first))
		{
			//auto __val = std::move(*__i);
			//_GLIBCXX_MOVE_BACKWARD3(__first, __i, __i + 1);
			//*__i = std::move(*__first);
			//*__first = std::move(__val);
			std::copy_backward(__first, __i, __i + 1);
			*__first = __val;
		}
		else
			__unguarded_linear_insert(__i);
	}
}




template<typename _RandomAccessIterator>
void
__final_insertion_sort(_RandomAccessIterator __first,
	_RandomAccessIterator __last)
{
	if (__last - __first > int(_S_threshold))
	{
		__insertion_sort(__first, __first + int(_S_threshold));
		__unguarded_insertion_sort(__first + int(_S_threshold), __last);
	}
	else
		__insertion_sort(__first, __last);
}

template<typename _Size>
inline _Size
__lg(_Size __n)
{
	_Size __k;
	for (__k = 0; __n != 0; __n >>= 1)
		++__k;
	return __k - 1;
}


template<typename _RandomAccessIterator>
inline void
__sort(_RandomAccessIterator __first, _RandomAccessIterator __last)
{
	if (__first != __last)
	{
		__introsort_loop(__first, __last, __lg(__last - __first) * 2);
		__final_insertion_sort(__first, __last);
	}
}


int main()
{
	srand(time(0));
	const size_t N = 30;
	std::vector<int> arr(N);
	for (int i = 0; i < N; ++i)
	{
		arr[i] = rand();
	}
	for (int i = 0; i < N; ++i)
	{
		std::cout << arr[i] << ' ';
	}
	std::cout << std::endl;
	//	std::vector<int>::iterator it = v.end();
	__insertion_sort(arr.begin(), arr.end());

	for (int i = 0; i < N; ++i)
	{
		std::cout << arr[i] << ' ';
	}
	return 0;
}

